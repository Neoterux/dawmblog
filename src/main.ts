import { createApp } from "vue";
import "./style.css";
import App from "./App.vue";
//import BootstrapVue3 from "bootstrap-vue-3";
import router from "@/router";
import AdminLayout from "@/views/layouts/AdminLayout.vue";
import FullLayout from "@views/layouts/FullLayout.vue";
import DefaultLayout from "@/views/layouts/DefaultLayout.vue";


//import "bootstrap/dist/css/bootstrap.css";
//import "bootstrap-vue-3/dist/bootstrap-vue-3.css";
import "./index.css"

createApp(App)
//  .use(BootstrapVue3)
  .use(router)
  .component("default-layout", DefaultLayout)
  .component("admin-layout", AdminLayout)
  .component("full-layout", FullLayout)
  .mount("#app");
