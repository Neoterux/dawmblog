import {
  createRouter,
  createWebHashHistory,
  type RouteRecordRaw,
} from "vue-router";
import MainView from "@/views/pages/MainView.vue";

const routes: RouteRecordRaw[] = [
  {
    name: "root",
    path: "/",
    component: MainView,
  },
  {
    name: "test",
    path: "/test",
    component: () => import("@/views/pages/MaterialTest.vue"),
    meta: {
      layout: 'admin-layout'
    }
  }
];

const router = createRouter({
  history: createWebHashHistory(),
  routes,
});

export default router;
