/** @type {import('tailwindcss').Config} */
const colors = require('tailwindcss/colors')
module.exports = {
  content: [
    "./index.html",
    "./src/**/*.{vue,js,ts,jsx,tsx}"
  ],
  theme: {
    extend: {
      colors: {
        surface: {
          DEFAULT: colors.gray[900],
          "0dp": colors.black,
          "1dp": colors.gray[800],
          "2dp": colors.gray[700],
          "3dp": colors.gray[600],
          "10dp": colors.gray[500],
        },
        primary: {
          DEFAULT: colors.yellow[500],
          dark: colors.yellow[600],
          light: colors.yellow[700]
        },
        secondary: {
          DEFAULT: colors.red[600],
          dark: colors.red[700],
          light: colors.red[300]
        },
        on: {
          primary: {
            DEFAULT: colors.black
          },
          secondary: {
            DEFAULT: colors.white
          }
        }
      },
    },
  },
  plugins: [],
}
